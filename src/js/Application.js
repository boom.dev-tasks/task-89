import EventEmitter from "eventemitter3";
import image from "../images/planet.svg";

export default class Application extends EventEmitter {
  static get events() {
    return {
      READY: "ready",
    };
  }

  constructor() {
    super();

    this._loading = document.querySelector('.progress');
    this._load();
    this.emit(Application.events.READY);
  }

  async _load() {
    this._startLoading();

    const API_URL = 'https://swapi.boom.dev/api/planets?page='
    for (let i = 1; i <= 6; i++) {
      const {results} = await fetch(API_URL + i).then(r => r.json());
      results.forEach(planet => {
        this._create(planet.name, planet.terrain, planet.population);
      });
    }

    this._stopLoading();
  }

  _create(name, terrain, population) {
    const box = document.createElement("div");
    box.classList.add("box");
    box.innerHTML = this._render({
      name: name,
      terrain: terrain,
      population: population,
    });

    document.body.querySelector(".main").appendChild(box);
  }

  _startLoading() {
    this._loading.style.display = 'block';
  }

  _stopLoading() {
    this._loading.style.display = 'none';
  }


  _render({ name, terrain, population }) {
    return `
<article class="media">
  <div class="media-left">
    <figure class="image is-64x64">
      <img src="${image}" alt="planet">
    </figure>
  </div>
  <div class="media-content">
    <div class="content">
    <h4>${name}</h4>
      <p>
        <span class="tag">${terrain}</span> <span class="tag">${population}</span>
        <br>
      </p>
    </div>
  </div>
</article>
    `;
  }
}
